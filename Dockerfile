FROM python:3-onbuild

COPY requirements.txt /tmp/

RUN pip install -r /tmp/requirements.txt
RUN apt-get update
RUN apt-get install -y redis-tools

COPY ./app /app

EXPOSE 5000
CMD python main.py
