from flask import Flask, jsonify, request, Response
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)

@app.route('/meeting')
def get_meeting():
    return jsonify(data['meeting'])

@app.route('/meeting/<string:fields>')
def get_meeting_id(fields):
    return jsonify(data['meeting'][fields])

@app.route('/instructors')
def get_instructors():
    return jsonify(data['instructors'])

@app.route('/instructors/<int:id>')
def get_instructors_id(id):
    return jsonify(data['instructors'][id])

@app.route('/instructors/<int:id>/<string:fields>')
def get_instructor_name(id, fields):
    return jsonify(data['instructors'][id][fields])

@app.route('/assignments', methods=['GET', 'POST'])
def get_post_assignment():
      if request.method == 'GET':
        return jsonify(data['assignments'])
      elif request.method == 'POST':
        request_data = request.get_json()
        data['assignments'].append(request_data)	
        return jsonify(data['assignments']) 		

@app.route('/assignments/<int:id>')
def get_assignments_id(id):
    return jsonify(data['assignments'][id])

@app.route('/assignments/<int:id>/<string:fields>')
def get_assignments_url(id, fields):
    return jsonify(data['assignments'][id][fields])
    	
@app.errorhandler(Exception)
def page_not_found(e):
    return "404 NOT FOUND", 404

#if __name__ == '__main__':    
#    app.run(debug=True, host='0.0.0.0')
